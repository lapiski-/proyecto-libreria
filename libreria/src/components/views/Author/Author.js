import React, {useState,useEffect} from 'react';
import { useHistory } from "react-router-dom";
import {BACKEND} from 'consts/backend';
import apiClient from 'utils/apiClient';
import ConfirmDialog from 'components/reusable-components/ConfirmDialog';
import { Formik, Field, Form } from 'formik';

function BookAuthor() {
        const [authors, setAuthors] = useState([]);
        const history = useHistory();
        const [isDialogOpen, setDialogOpen] = useState(false);
        const [idAuthor, setIdAuthor] = useState(false);

        useEffect(() => {
            apiClient.get(`${BACKEND}/authors`).then((data) => {
                setAuthors(data);
                console.log(data);
            });
            
        }, []);

        const deleteAuthor = (e) => {
            setIdAuthor(e.target.getAttribute("data-id"));
            setDialogOpen(true);
        };

        const handleConfirmDialog = async(confirm) => {
            if(confirm === "ok"){
                //alert("Entrando Eliminar Autor " + idAuthor);
                await apiClient.del(`${BACKEND}/authors/${idAuthor}`);
                history.push(`/authors/`);
            }
            setDialogOpen(false);
            window.location.reload();
        };

        return (
            <div className="container page-authors"> 
                <div className="add-author">
                    <div className="books-header">
                        <h3><span>All</span> Authors</h3>
                    </div>

                    {authors.map((author, i) => {
                        return <div className="item-author" key={i}><span>{author.name}</span><span className="btn btn-outline-secondary btn-sm" onClick={deleteAuthor} data-id={author.id}>Delete</span> </div>;
                    })}
                </div>

                <ConfirmDialog openDialog={isDialogOpen} confirmDialog={(confirm) => handleConfirmDialog(confirm)} title="Author" question="¿Está seguro de que desea eliminar al autor?"/>
                <Formik
                    initialValues={{
                        authorName: '',
                    }}
                    onSubmit={async(values) => {
                        //await new Promise((r) => setTimeout(r, 500));
                        //alert(values.categoryName);
                        await apiClient.post(`${BACKEND}/authors`, JSON.stringify({name: values.authorName}));
                        window.location.reload();
                    }}
                    >
                    <Form>
                        <div className="books-header">
                            <h3><span>Add</span> Author</h3>
                        </div>
                        <div className="autor-name">
                            <div>
                                <label htmlFor="authorName">Author Name</label>
                            </div>
                            <div>
                                <Field id="authorName" name="authorName"/>
                            </div>
                        </div>
                        <button type="submit" className="btn btn-outline-primary">Add Author</button>
                    </Form>
                </Formik>
            </div> 
        );
    
}

export default BookAuthor;
