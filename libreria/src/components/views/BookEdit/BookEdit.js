import React, {useState,useEffect} from 'react';
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import StarsInput from 'components/reusable-components/StarsInput';
import apiClient from 'utils/apiClient';
import {BACKEND} from 'consts/backend';
import { useParams } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import useCategories from 'hooks/useCategories';
import useAuthors from 'hooks/useAuthors';


function BookEdit() {
    const [book, setBook] = useState("");
    const [title, setTitle] = useState();
    const [image, setImage] = useState("");
    const [description, setDescription] = useState("");
    const [selectedCategories, setSelectedCategories] = useState("");
    const [selectedAuthors, setSelectedAuthors] = useState(null);
    const {data: categories} = useCategories(null);
    const {data: authors} = useAuthors();
    const [value, onChange] = useState(new Date());
    const [stars, setStars] = useState(0);

    const params = useParams();
    const history = useHistory();


    useEffect(() => {
        apiClient.get(`${BACKEND}/books/${params.id}`).then((data) => {
            console.log("book ==>",data);
            setBook(data);
        });
    }, [params.id]);

    function handleTitleChange(event) {
        //console.log(event.target.value);
        setTitle(event.target.value);
    }

    function handleDescriptionChange(event) {
        setDescription(event.target.value);
    }

    function handleImageChange(event) {
        let reader = new FileReader();
        reader.onload = function() {
            setImage(reader.result);
        };
        reader.readAsDataURL(event.target.files[0]);     
    }



    async function handleSubmit(event) {
        event.preventDefault();
        try {
            const data = {
                title: title ? title : book.title,  
                score: stars !== 0 ? stars : book.score,
                categories: {
                    "0": {
                        "id": selectedCategories ? selectedCategories.id : book.categories[0].id,
                        "name": selectedCategories ? selectedCategories.name : book.categories[0].name,
                    }
                },
                authors: {
                    "0": {
                        "id": selectedAuthors ? selectedAuthors.id : book.authors[0].id,
                        "name": selectedAuthors ? selectedAuthors.name : book.authors[0].name,
                    }
                },
                description: description ? description : event.target[1].value,
                base64Image: image,
            };       
            await apiClient.post(`${BACKEND}/books/${book.id}`, JSON.stringify(data));
            history.push(`/books/`);
           
        } catch (error) {
            console.log(error);
        }
    }

    if (!book) {
        return "Cargando...";
    } 
    else {
        return (
            <div className="container"> 
                <div className="books-header">
                    <h3><span>Edit</span> Book</h3>
                </div>
                <div className="row">
                    <form onSubmit={handleSubmit} className="">
                        <h4>Formulario Edit Book</h4>
                        <div className="form-group">
                            <label>Título</label>
                            <input type="text" value={title ? title : book.title} onChange={handleTitleChange} name="title"></input>
                        </div>  
                        <div className="form-group">
                            <label>Description:</label>
                            <textarea type="text" onChange={handleDescriptionChange} value={description !== '' ? description : book.description}>{book.description}</textarea>
                        </div>
                        <div className="form-group">
                            <DatePicker
                                onChange={onChange}
                                value={value}
                            />
                        </div>
                        <div className="form-group">
                            <label>Categoría actual</label>
                            <Select 
                                className="selector"
                                placeholder={'Select Category'}
                                defaultValue={selectedCategories ? selectedCategories : book.categories}
                                onChange={setSelectedCategories}
                                options={categories} 
                                getOptionLabel={option => option.name} 
                                getOptionValue={option => option.id}
                                // isMulti
                            />
                        </div>
                        <div className="form-group">
                            <label required>Author</label>
                            <Select 
                                className="selector"
                                placeholder={'Select Author'}
                                defaultValue={selectedAuthors ? selectedAuthors : book.authors}
                                onChange={setSelectedAuthors}
                                options={authors} 
                                getOptionLabel={option => option.name} 
                                getOptionValue={option => option.id}
                                // isMulti
                            />
                        </div>
                        <div className="form-group">
                            <label>Puntuación</label>
                            <div className="score">
                                <StarsInput bookScore={book.score} sendStars={(score) =>setStars(score)} isDetail={false}/>
                            </div>
                        </div>
                        <div className="form-group">
                            <img className="img-fluid" src={image ? image : book.image} alt={book.title} />
                            <p>Seleccionar imagen:</p>
                            <input type="file" onChange={handleImageChange}/>
                        </div>
                        <div className="form-group">
                            <button className="btn btn-outline-primary" type="submit">Guardar</button>
                        </div>
                    </form>
                </div>
            </div> 
        );
    }
}

export default BookEdit;
