import React, {useState} from 'react';
import logo from 'assets/images/logo-letters.png';
import {MAGIC_WORD} from 'consts/app';
import useAuthContext from 'hooks/useAuthContext';

function Login() {

    const [magicWord, setMagicWord] = useState('');
    const {login} = useAuthContext();
  
    function handleMagicWordChange(event) {
        setMagicWord(event.target.value);
    }
  
    function handleSubmit(event) {
        event.preventDefault();
        if (magicWord !== MAGIC_WORD) {
            alert('😝 La palabara clave que has introducido no es la correcta');
        } else {
            login(magicWord);
        }
    }

    return (
        <div className="container text-center wrapper-login">
            <div className="login">
                <div className="books-header">
                    <h3><span>Hi to</span><img src={logo} className="logo" alt="logo" /> </h3>
                </div>
                <form onSubmit={handleSubmit}>
                    <div class="form-group">
                        <label>Introduce la contraseña para acceder</label>
                        <input type="text" magicWord={magicWord} onChange={handleMagicWordChange}></input> 
                    </div>
                    <div>
                        <button className="btn btn-outline-primary" type="submit">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Login;