import React from 'react';
import { useHistory } from "react-router-dom";


function CardBook({book, cardIdDelete}) {

    const history = useHistory();

    const handleDetailBook = (book) => {
        const idBookSelected = book.id;
        history.push(`/books/${idBookSelected}`);
    };

    const handleEditBook = (book) => {
        console.log("book ==>",book.id);
        const idBookSelected = book.id;
        history.push(`/books/${idBookSelected}/edit`);
    };

    const handleDeleteBook = (book) => {
        //alert("Hijo ==> " + book.id);
        return cardIdDelete(book.id);
    };

    return (  
        <div className="card" id={book.id} key={book.id}>
            <div className="card-body" onClick={() => handleDetailBook(book)}>
                <div></div>
                <img className="card-img-top" src={book.image} alt={book.title} />
                <h5 className="card-title" >{book.title}</h5>
                {book.categories.map((category) => {
                    return <span key={category.id} className="category">{category.name}</span>;
                })}
            </div>
            <div className="card-wrapper-btn">
                <span onClick={() => handleDeleteBook(book)} className="btn btn-outline-secondary btn-sm">Delete</span>
                <span onClick={() => handleEditBook(book)} className="btn btn-outline-primary btn-sm">Editar</span>
            </div>
        </div>
    );
}



export default CardBook;