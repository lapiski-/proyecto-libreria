import React, {useState} from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

function ViewFormat({modoVista}) {

    const [state, setState] = useState({
        viewFormat: true,
    });

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
        //alert(event.target.checked);
        return modoVista(event.target.checked);
    };
    
    return (  
        <div class="view-format-column">
            <div class="text"> Vista</div> 
            <div class="item-container">
                <div className={`${state.viewFormat ? 'item' : 'item is-select'}`}>3</div> 
            </div>
            <FormControlLabel 
                className="switch-view-format"
                control={<Switch size="small" checked={state.viewFormat} onChange={handleChange} name="viewFormat" />}
            />
            <div class="item-container">
                <div className={`${state.viewFormat ? 'item is-select' : 'item"'}`}>4</div> 
            </div>
        </div>
    );
}



export default ViewFormat;