import React, {useState, useEffect} from 'react';
import ConfirmDialog from 'components/reusable-components/ConfirmDialog';
import Select from 'react-select';
import CardBook from './components/CardBook';
import ViewFormat from './components/ViewFormat';
import {BACKEND} from 'consts/backend';
import apiClient from 'utils/apiClient';
import useCategories from 'hooks/useCategories';
import useAuthors from 'hooks/useAuthors';


function Books() {

    const [books, setBooks] = useState([]);
    const [selectedCategories, setSelectedCategories] = useState("");
    const [selectedAuthors, setSelectedAuthors] = useState("");
    const {data: categories} = useCategories();
    const {data: authors} = useAuthors();
    const [isDialogOpen, setDialogOpen] = useState(false);
    const [deletedBookId, setDeletedBookId] = useState("");
    const [typeView, setTypeView] = useState(false);

    useEffect(() => {
        apiClient.get(`${BACKEND}/books`).then((data) => {
            //console.log("data books ==>",data);
            setBooks(data);
        });
    }, []);

    const booksToShow = function() {
        if (!books) {
            return (
                <div className="container">
                    <p>Cargando libros...</p>
                </div>
            );
        } 
        if (selectedCategories) {
            //console.log("selectedCategories ==>",selectedCategories.id);
            return books.filter((book) => {
                //console.log("book ==>",book);
                return book.categories.some((cat) => cat.id === selectedCategories.id);
            });
        } 
        if (selectedAuthors) {
            return books.filter((book) => {
                return book.authors.some((author) => author.id === selectedAuthors.id);
            });
        } 
        else {
            return books;
        }
    };

    const handleCardDelete = (cardDelete) => {
        //alert("Padre ==> " + cardDelete);
        setDialogOpen(true);
        setDeletedBookId(cardDelete);
    };


    const handleConfirmDialog = (confirm) => {
        if(confirm === "ok"){
            apiClient.del(`${BACKEND}/books/${deletedBookId}`).then(() => {
                apiClient.get(`${BACKEND}/books`).then((data) => {
                    setBooks(data);
                });
            });
        }
        setDialogOpen(false);
    };

    const handleTypeView = (e) => {
       //alert("Estado del Switch " + e);
       setTypeView(!e);
    };

    
    return (
        <div className="container">
            <div className="books-header">
                <h3><span>Popular</span> Genres</h3>
                <Select 
                    className="selector"
                    defaultValue={selectedAuthors}
                    onChange={setSelectedAuthors}
                    options={authors} 
                    placeholder="Select Category"
                    getOptionLabel={option => option.name} 
                    getOptionValue={option => option.id}
                />
                <Select 
                    className="selector"
                    defaultValue={selectedCategories}
                    onChange={setSelectedCategories}
                    options={categories} 
                    placeholder="Select Author"
                    getOptionLabel={option => option.name} 
                    getOptionValue={option => option.id}
                    // isMulti
                />
            </div>

            <ViewFormat modoVista={handleTypeView} />

            <div className="row wrapper-cards">
                {booksToShow().map(book => (      
                    <div className={`${typeView ? 'col-12 col-md-6 col-lg-4 col-xl-4' : 'col-6 col-md-6 col-lg-4 col-xl-3'}`} key={book.id} data-type={typeView}>
                        <CardBook book={book} key={book.id} cardIdDelete={handleCardDelete}/>
                    </div>
                ))}
            </div>
            <ConfirmDialog openDialog={isDialogOpen} confirmDialog={(confirm) => handleConfirmDialog(confirm)} title="Book" question="¿Está seguro de que desea eliminar el libro?"/>
        </div>
    );
}
export default Books;