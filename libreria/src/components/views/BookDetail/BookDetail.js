import React, {useState,useEffect} from 'react';
import StarsInput from 'components/reusable-components/StarsInput';
import apiClient from 'utils/apiClient';
import {BACKEND} from 'consts/backend';
import { useParams } from 'react-router-dom';
import Moment from 'moment';
import 'moment/locale/es';

Moment.locale('es');


function BookDetail() {
    const [book, setBook] = useState(undefined);
    const params = useParams();

    useEffect(() => {
        apiClient.get(`${BACKEND}/books/${params.id}`).then((data) => {
            console.log("data ==>",data);
            setBook(data);
        });
    }, [params.id]);


    if (!book) {
        return (
            <div className="container">No hay libro</div> 
        );
    } 
    else {
        return (
            <div className="container book-detail"> 
                <div className="books-header">
                    <h3><span>Book</span> Detail</h3>
                </div>

                <div className="book" >
                    <img className="card-img-top" src={book.image} alt={book.title} />
                    <div className="wrapper-text">
                        <h5 className="item">Title <span>{book.title}</span> </h5>
                        <h5 className="item">Category <span>{book.categories && book.categories[0] && book.categories[0].name}</span> </h5>
                        <h5 className="item">Author <span>{book.authors && book.authors[0] && book.authors[0].name}</span> </h5>
                        <h5 className="item">Fecha <span>{Moment(book.createdAt).format('DD/MM/YYYY')}</span> </h5>
                        <StarsInput bookScore={book.score} isDetail={true}/>
                        <p className="description">{book.description}</p>
                    </div>
                </div>
            </div> 
        );
    }
}

export default BookDetail;
