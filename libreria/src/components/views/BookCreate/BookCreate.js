import React from 'react';

import FormCreateBook from 'components/Form/FormCreateBook';

function BookCreate() {

    return (
        <div className="container"> 
            <div className="books-header">
                <h3><span>Add</span> Book</h3>
            </div>
            <FormCreateBook/> 
        </div>
    );
}

export default BookCreate;