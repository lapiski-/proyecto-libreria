import React, {useState,useEffect} from 'react';
import { useHistory } from "react-router-dom";
import {BACKEND} from 'consts/backend';
import apiClient from 'utils/apiClient';
import ConfirmDialog from 'components/reusable-components/ConfirmDialog';
import { Formik, Field, Form } from 'formik';


function BookCategories() {
        const [categories, setCategories] = useState([]);
        const history = useHistory();
        const [isDialogOpen, setDialogOpen] = useState(false);
        const [idCategory, setIdCategory] = useState(false);

        useEffect(() => {
            apiClient.get(`${BACKEND}/categories`).then((data) => {
                setCategories(data);
            });
        }, []);

        const deleteCategory = (e) => {
            setIdCategory(e.target.getAttribute("data-id"));
            setDialogOpen(true);
        };


        const handleConfirmDialog = async(confirm) => {
            console.log("confirm ==>",confirm);
            if(confirm === "ok"){
                //alert("Entrando " + idCategory);
                await apiClient.del(`${BACKEND}/categories/${idCategory}`);
                history.push(`/categories/`);
            }
            setDialogOpen(false);
            window.location.reload();
        };


        return (
            <div className="container page-categories"> 
                <div className="books-header">
                    <h3><span>All</span> Categories</h3>
                </div>
               {/* {console.log("categories ==>",categories)} */}
               {categories.map(category => {
                   return <div className="item-category" key={category.id}><span>{category.name}</span><span className="btn btn-outline-secondary btn-sm" onClick={deleteCategory} data-id={category.id}>Delete</span> </div>;
               })}

                <ConfirmDialog openDialog={isDialogOpen} confirmDialog={(confirm) => handleConfirmDialog(confirm)} title="Category" question="¿Está seguro de que desea eliminar la categoría?"/>

                <Formik
                    initialValues={{
                        categoryName: '',
                    }}
                    onSubmit={async(values) => {
                        //await new Promise((r) => setTimeout(r, 500));
                        //alert(values.categoryName);
                        await apiClient.post(`${BACKEND}/categories`, JSON.stringify({name: values.categoryName}));
                        window.location.reload();
                    }}
                    >
                    <Form>
                        <div className="books-header">
                            <h3><span>Add</span> Category</h3>
                        </div>
                        <div className="autor-name">
                            <div>
                                <label htmlFor="categoryName">Author Category</label>
                            </div>
                            <div>
                                <Field id="categoryName" name="categoryName"/>
                            </div>
                        </div>
                        <button type="submit" className="btn btn-outline-primary">Add Category</button>
                    </Form>
                </Formik>

            </div> 
        );
    
}

export default BookCategories;
