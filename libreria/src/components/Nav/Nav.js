import React, {useState} from "react";
import {NavLink} from "react-router-dom";
import {BOOKS, BOOK_CREATE, LOGOUT, CATEGORIES, AUTHORS} from 'config/routes';
import logo from './logo.png';
import useAuthContext from 'hooks/useAuthContext';
import IconBook from '@material-ui/icons/LibraryBooksOutlined';
import IconAddBook from '@material-ui/icons/AddToPhotosOutlined';
import IconExit from '@material-ui/icons/ExitToApp';
import IconAuthors from '@material-ui/icons/PeopleAltOutlined';
import IconCategories from '@material-ui/icons/FormatListNumberedRounded';
import IconMenuOpen from '@material-ui/icons/Menu';
import IconMenuClose from '@material-ui/icons/Close';


function Nav() {

    const {isAuthenticated} = useAuthContext();
    const [menuToggle, setMenuToggle] = useState(false);

    const handleMenuToggle = () => {
        if (menuToggle == false){
            setMenuToggle(true);
            document.body.classList.add("menu-open");
        } 
        else{
            setMenuToggle(false);
            document.body.classList.remove("menu-open");
        };
    };

    return (
        <div className="container">

            {isAuthenticated ?    
                <header>
                    <img src={logo} className="logo" alt="logo" /> 
                    {menuToggle 
                    ?  
                        <IconMenuClose className={`${menuToggle ? 'icon-menu is-select' : 'icon-menu'}`} onClick={handleMenuToggle}/>
                    : 
                        <IconMenuOpen className={`${menuToggle ? 'icon-menu is-select' : 'icon-menu'}`} onClick={handleMenuToggle}/>
                    }

                    
                    <nav className="main-nav">
                        <div class="link-background"></div>
                        <ul className="container">
                            <li>
                                <NavLink to={BOOKS} activeClassName="active" exact onClick={handleMenuToggle}>
                                    <span>Books</span>
                                    <IconBook/>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to={BOOK_CREATE} activeClassName="active" exact onClick={handleMenuToggle}>
                                    <span>Add Book</span>
                                    <IconAddBook/>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to={CATEGORIES} activeClassName="active" exact onClick={handleMenuToggle}>
                                    <span>Categories</span>
                                    <IconCategories/>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to={AUTHORS} activeClassName="active" exact onClick={handleMenuToggle}>
                                    <span>Authors</span>
                                    <IconAuthors/>
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to={LOGOUT} activeClassName="active" exact onClick={handleMenuToggle}>
                                    <span>Logout</span>
                                    <IconExit/>
                                </NavLink>
                            </li>
                        </ul>
                    </nav>
                </header>
                    
                
            : <div></div>
            } 
        </div>
    );
}

export default Nav;