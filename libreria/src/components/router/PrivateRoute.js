import React from 'react';
import PropTypes from 'prop-types';
import {Redirect, Route} from 'react-router-dom';
import {LOGIN} from 'config/routes';
import useAuthContext from  'hooks/useAuthContext';

function PrivateRoute({component: Component, ...rest}) {
    const {isAuthenticated} = useAuthContext();

    if (!isAuthenticated) {
        return <Redirect to={LOGIN} />;
    } 

    return (
        <Route
          {...rest}
          render={(props) => (isAuthenticated ? <Component {...props} /> : <Redirect to={LOGIN} />)}
        />
      );

};


PrivateRoute.propTypes = {
    component: PropTypes.func.isRequired
  };
  
  export default PrivateRoute;
