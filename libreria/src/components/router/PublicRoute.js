import React from 'react';
import PropTypes from 'prop-types';
import {Redirect, Route} from 'react-router-dom';
import {BOOKS} from 'config/routes';
import useAuthContext from  'hooks/useAuthContext';

function PublicRoute({component: Component, ...rest}) {
    const {isAuthenticated} = useAuthContext();
  
    return (
      <Route
        {...rest}
        render={(props) => (isAuthenticated ? <Redirect to={BOOKS} /> : <Component {...props} />)}
      />
    );
  }
  
  PublicRoute.propTypes = {
    component: PropTypes.func.isRequired
  };
  
  export default PublicRoute;
  