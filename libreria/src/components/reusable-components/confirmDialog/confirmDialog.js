import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import PropTypes from "prop-types";

function ConfirmDialog({openDialog,confirmDialog,title,question}) {
    //console.log("props ==>",props);
    const handleClose = (e) => {
        return confirmDialog(e.target.parentElement.getAttribute("data-action"));
    };

    return (
        <div>
            
            <Dialog 
                open={openDialog}
                onClose={handleClose}
            >
                <div className="books-header">
                    <h3><span>{title}</span> Delete</h3>
                </div>
                <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
                    {question}
                </DialogTitle>
                <DialogActions>
                    <Button
                        autoFocus
                        onClick={handleClose}
                        data-action="cancel"
                        className="btn btn-outline-secondary btn-sm"
                    >
                    Cancel
                    </Button>
                    <Button 
                        onClick={handleClose} 
                        data-action="ok"
                        className="btn btn-outline-primary btn-sm">
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

ConfirmDialog.propTypes = {
    openDialog: PropTypes.bool.isRequired,
    confirmDialog: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    question: PropTypes.string.isRequired
};

export default ConfirmDialog;