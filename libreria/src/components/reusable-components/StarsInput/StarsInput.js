import React, {useState} from 'react';
import PropTypes from "prop-types";

function StarsInput({bookScore, sendStars, isDetail}) {
    const [stars, setStars] = useState(bookScore);
    // function handleScore(event) {
    //     if(isDetail){
    //         return false;
    //     }
    //     let score = event.currentTarget.getAttribute("data-score");
    //     setStars(score);
    //     sendStars(score);
    // }

    function handleScore(score) {
        if(isDetail){
            return false;
        }
        setStars(score);
        sendStars(score);
    }

    return (
        <div className="score">
            {[...Array(5)].map((i,index) => {       
                index ++; 
                return (       
                    <span key={index} onClick={() => handleScore(index)}  className={index <= (stars !== 0 ? stars : bookScore) ? "on" : "off"}></span>  
                    //<span key={index} data-score={index} onClick={(handleScore)}  className={index <= (stars !== 0 ? stars : bookScore) ? "on" : "off"}></span>      
                );
            })}
        </div>
    );
}

StarsInput.propTypes = {
    bookScore: PropTypes.number.isRequired,
    isDetail: PropTypes.bool.isRequired,
    sendStars: PropTypes.func.isRequired,
};

export default StarsInput;
