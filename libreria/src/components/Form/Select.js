import React from 'react';
import Select from 'react-select';

export default ({ onChange, options, value, className, placeholder, label }) => {

    const defaultValue = (options, value,) => {
        return options ? options.find(option => option.value === value) : "";
    };

    return (
        <div className={className}>
            <label>{label}</label>
            <Select
                placeholder={placeholder}
                value={defaultValue(options, value)}
                getOptionLabel={(option) => option.name}
                getOptionValue={(option) => option.id}
                getNewOptionData={(optionValue) => ({
                  id: "",
                  name: optionValue,
                })}
                onChange={value => {
                    //console.log("value ==>",value);
                    onChange(value);
                }} options={options} />
        </div>

    );
};