import React, {useState} from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import StarsInput from 'components/reusable-components/StarsInput';
import FormikControl from './FormikControl.js';
import useCategories from 'hooks/useCategories';
import useAuthors from 'hooks/useAuthors.js';
import apiClient from 'utils/apiClient';
import {BACKEND} from 'consts/backend';
import { useHistory } from "react-router-dom";


function FormCreateBook(book) {

    const {data: optionsCategory} = useCategories();
    const {data: optionsAuthors} = useAuthors();
    const [stars, setStars] = useState(0);
    const [image, setImage] = useState("");
    const history = useHistory();

    const initialValues = {
        title: '',
        description: '',
        score: stars,
        author: [],
        category: [],
        base64Image: '',
    };

    const validationSchema = Yup.object({
        title: Yup.string()
          .required('Required'),
        description: Yup.string().required('Required')
    });


    const onSubmit = async values => {
        const body = {
            title: values.title,
            description: values.description,
            //score: parseInt(values.score),
            score: stars,
            categories: {
                "0": {
                    "id": values.category.id,
                    "name": values.category.name
                }
            },
            authors: {
                "0": {
                    "id": values.author.id,
                    "name": values.author.name
                }
            },
            base64Image: image,
        };
        //console.log('Cuerpo generado ==>', body);
        await apiClient.post(`${BACKEND}/books`, JSON.stringify(body));
        history.push(`/books/`);
    };

    // const scoreOptions = [
    //     { key: 1, value: 1 },
    //     { key: 2, value: 2 },
    //     { key: 3, value: 3 },
    //     { key: 4, value: 4 },
    //     { key: 5, value: 5 },
    // ];

    function handleImageChange(event) {
        let reader = new FileReader();
        reader.onload = function() {
            setImage(reader.result);
            console.log(" reader.result",reader.result);
        };
        reader.readAsDataURL(event.target.files[0]);    
        console.log("image ===>",image); 
    }

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
        >
            {formik => {
                return (
                   <Form>
                        <FormikControl
                            control='input'
                            type='text'
                            label='Title'
                            name='title'
                        />
                        <FormikControl
                            control='textarea'
                            type='text'
                            label='Description'
                            name='description'
                        />
                        <FormikControl
                            control='select'
                            label='Selecciona Category'
                            className="form-control"
                            type='select'
                            placeholder='Select Category'
                            name='category'
                            onChange={(value)=>formik.setFieldValue('category',value)}
                            options={optionsCategory}
                            value={formik.values.category}
                        />
                        <FormikControl
                            control='select'
                            label='Selecciona Author'
                            className="form-control"
                            type='select'
                            placeholder='Select Author'
                            name='author'
                            onChange={value=>formik.setFieldValue('author',value)}
                            options={optionsAuthors}
                            value={formik.values.author}
                        />
                        {/* <FormikControl
                            control='radio'
                            type='radio'
                            label='Score'
                            name='score'
                            options={scoreOptions}
                        /> */}
                        <div className="form-group">
                            <label>Puntuación</label>
                            <div className="score">
                                <StarsInput bookScore={book.score} sendStars={(score) =>setStars(score)}/>
                            </div>
                        </div>
                        
                        <div className="form-group">
                            <label>Seleccionar imagen:</label>
                            <input type="file" onChange={handleImageChange}/>
                            <img src={image}/>
                        </div>
        
                        <button className="btn btn-outline-primary" type='submit' disabled={!formik.isValid}>Submit</button>
                    </Form>
                );
            }}
        </Formik>
    );
}
export default FormCreateBook;