import React from "react";
import {
  BrowserRouter as Router,
  Switch,
} from "react-router-dom";
import PrivateRoute from './components/router/PrivateRoute';
import PublicRoute from './components/router/PublicRoute';
import Nav from './components/Nav/Nav';
import Books from './components/views/Books';
import BookEdit from './components/views/BookEdit';
import BookDetail from './components/views/BookDetail';
import BookCreate from './components/views/BookCreate';
import Categories from './components/views/Categories';
import Author from './components/views/Author';
import Logout from './components/views/Logout';
import Login from './components/views/Login/';
import AuthProvider from './contexts/auth';


import {
    LOGIN,
    BOOKS, 
    BOOK_EDIT, 
    BOOK_CREATE, 
    LOGOUT,
    BOOK_DETAIL,
    CATEGORIES,
    AUTHORS
} from './config/routes';

export default function App() {

    
    return (
        <AuthProvider>
            <Router>
                <div className="App">
                   
                    <Nav />
        
                    <Switch>
                        <PublicRoute path={LOGIN} component={Login} exact/> 
                        <PrivateRoute path={BOOKS} component={Books} exact />
                        <PrivateRoute path={BOOK_CREATE} component={BookCreate} exact />
                        <PrivateRoute path={BOOK_EDIT} component={BookEdit} exact />
                        <PrivateRoute path={BOOK_DETAIL} component={BookDetail} exact />
                        <PrivateRoute path={CATEGORIES} component={Categories} exact />
                        <PrivateRoute path={AUTHORS} component={Author} exact />
                        <PrivateRoute path={LOGOUT} component={Logout} exact />
                        <PrivateRoute path={LOGIN} component={Login} exact/> 
                    </Switch>
                </div>
            </Router>
        </AuthProvider>
    );
}